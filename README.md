# splitArray Function

![Twig Logo](https://cdn.twig-world.com/twig/twig/prod/static/common/images/twig-logo.ebe23c9de850.png "Twig Logo")

_Given an array of length >= 0, and a positive integer N, return the contents of the array divided into N equally sized arrays._

_Where the size of the original array cannot be divided equally by N, the final part should have length equal to the remain_


## To edit
Open `splitArray.js` in your preferred text editor. 

Change `array` to desired length

Change `number` to desired split length

``` javascript
let array = [1, 2, 3, 4, 5, 6, 7];
let number = 4;
```

## To execute
Open `splitArray.js` in NODE on terminal (on Linux/iOS) or Powershell (command prompt on Windows).

With the following command 
`node splitArray.js `