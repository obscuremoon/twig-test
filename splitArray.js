"use strict";

let array = [1, 2, 3, 4, 5, 6, 7];
let number = 4;

/**
 * Given an array of length >= 0, and a positive integer N, return the contents of the array
 * divided into N equally sized arrays.
 *
 * Where the size of the original array cannot be divided equally by N, the final part should have
 * length equal to the remainder.
 *
 * @param a {Array} Array to split
 * @param n {Integer} Size of each new array
 * @return results {Array} Array to return containing the new arrays
 */
function splitArray(a, n){

	let results = [];
	let sizeOfArray = 0;

    // Validating a
    if(!Array.isArray(a) || a.length < 0){
    	throw new Error("a must be an array >= 0");
    }

	if(a.length <= 1){
		results.push(a);
		return results;
	}

    // Validating n
    if( typeof n !== 'number' || n <= 0 ){
		throw new Error("n must be a positive integer. " + n + " is not a positive integer");
	}

	// Size of each new array
    sizeOfArray = Math.ceil(a.length / n);

    // Checks if a divides equally and switched to round down if false
    if(Math.ceil(a.length / sizeOfArray) < n){
    		sizeOfArray = Math.floor(a.length / n);
    }

    // splices and pushes a into results 
    for (let i = 0; i < number-1; i++) {
    	results.push(a.splice(0, sizeOfArray));
    }

    // Adds the remainder to the resulting array
    results.push(a);
    
    return results;
}

let newArray = splitArray(array, number);
console.log(newArray);